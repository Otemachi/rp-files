# Sunshine RP Background
You are a resident of the beautiful city of [Numazu, Shizuoka](https://en.wikipedia.org/wiki/Numazu)
You are an ordinary resident.
Unbeknownst to you, several events are going to come your way
# Important Info
## The T R A I N
An entity that manifests when a carbon-based lifeform says the forbidden word "tra¡n" (note: `i` replaced with `¡`). Will manifest in [Bungo-Mori Rail Yard](https://en.wikipedia.org/wiki/Bungo-Mori_Station) and promptly come to the person who says the word (or a random place) in **3 minutes**, normally causes earthquake after impact. Controlled by Kanan.

<img src="https://vignette.wikia.nocookie.net/love-live/images/b/bb/084_Happy_Party_Train.png" alt="Fig 1. Picture of the T R A I N, April 2017." width="400"/>

> **Fig 1. Picture of the T R A I N, April 2017.** 
## The Blane
An entity that manifests when a carbon-based lifeform says the word "wonderfuI" (note: `l` replaced with capital `I`), causes a plane to manifest, taking off from [Haneda Airport](https://en.wikipedia.org/wiki/Haneda_Airport) before crashing in a random place in most cases.

<img src="https://i.imgur.com/DO1BXxY.png" alt="Fig 2. Picture of the Blane, October 2011." width="400"/>

> **Fig 2. Picture of the Blane, October 2011.**